<?php
	//jQuery
	function include_custom_js() {
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-3.2.1.min.js', array(), null, true);
		wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array(), null, true);
		wp_register_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array(), null, true);
		wp_register_script( 'scripts', get_stylesheet_directory_uri() . '/js/script.js', array(), null, true);
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'popper' );
		wp_enqueue_script( 'bootstrap' );
		wp_enqueue_script( 'scripts' );
	}
	add_action('wp_enqueue_scripts', 'include_custom_js');
	//Font awesome
	add_action('wp_enqueue_scripts', 'custom_load_font_awesome');
	function custom_load_font_awesome() {
		wp_enqueue_style( 'font-awesome-pro', '//pro.fontawesome.com/releases/v5.8.1/css/all.css' );
	}
	//Google fonts
	function themeslug_fonts_url() {
		$fonts_url = '';
		$fonts     = array();
		$subsets   = 'latin,latin-ext';
		$fonts[] = 'Montserrat:400,600,700,900';
		if ( $fonts ) {
			$fonts_url = add_query_arg( array(
				'family' => urlencode( implode( '|', $fonts ) ),
				'subset' => urlencode( $subsets ),
			), 'https://fonts.googleapis.com/css' );
		}
		return esc_url_raw( $fonts_url );
	}
	function themeslug_scripts() {
		wp_enqueue_style( 'themeslug-fonts', themeslug_fonts_url(), array(), null );
	}
	add_action( 'wp_enqueue_scripts', 'themeslug_scripts' );
	//Register nav
	function register_my_menus() {
		register_nav_menus(
			array(
				'main_nav' => __( 'Main navigation' ),
				'footer_nav' => __( 'Footer menu 1' ),
				'footer_nav_last' => __( 'Footer menu 2' )
			)
		);
	}
	add_action( 'init', 'register_my_menus' );
	//Activate options
	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'Theme Global Settings',
			'menu_title'	=> 'Global Settings',
			'menu_slug' 	=> 'theme-global-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
	}
	//Login logo
	function my_login_logo() { ?>
		<style type="text/css">
			#login h1 a,
			.login h1 a {
				width: 80px;
				height: 80px;
				background-size: 80px 80px;
				background-repeat: no-repeat;
				background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo-core.svg);
			}
		</style>
	<?php }
	add_action( 'login_enqueue_scripts', 'my_login_logo' );
	//Gravity forms jQuery fix
	add_filter("gform_init_scripts_footer", "init_scripts");
		function init_scripts() {
		return true;
	}
	//Kill emojis
	function disable_wp_emojicons() {
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
	}
	add_action( 'init', 'disable_wp_emojicons' );
	add_filter( 'emoji_svg_url', '__return_false' );
	function disable_emojicons_tinymce( $plugins ) {
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		} else {
			return array();
		}
	}
	//Pagination
	function wpbeginner_numeric_posts_nav() {
	if( is_singular() )
		return;
		global $wp_query;
	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;
		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max   = intval( $wp_query->max_num_pages );
	/** Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;
	/** Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}
	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}
	echo '<div class="pagination"><ul>' . "\n";
	/** Previous Post Link */
	if ( get_previous_posts_link() )
	printf( '<li>%s</li>' . "\n", get_previous_posts_link('<i class="far fa-chevron-double-left"></i>') );
	/** Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
		}
		/** Link to current page, plus 2 pages in either direction if necessary */
		sort( $links );
		foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}
	/** Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
		echo '<li>…</li>' . "\n";
		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}
	/** Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link('<i class="far fa-chevron-double-right"></i>') );
		echo '</ul></div>' . "\n";
	}
	//Campaign CPT
	function cpt_campaigns() {
		$labels = array(
			'name'                  => _x( 'Campaigns', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Campaign', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Campaigns', 'text_domain' ),
			'name_admin_bar'        => __( 'Campaigns', 'text_domain' ),
			'archives'              => __( 'Item Archives', 'text_domain' ),
			'attributes'            => __( 'Item Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
			'all_items'             => __( 'All Campaigns', 'text_domain' ),
			'add_new_item'          => __( 'Add Campaign', 'text_domain' ),
			'add_new'               => __( 'Add Campaign', 'text_domain' ),
			'new_item'              => __( 'New Item', 'text_domain' ),
			'edit_item'             => __( 'Edit Item', 'text_domain' ),
			'update_item'           => __( 'Update Item', 'text_domain' ),
			'view_item'             => __( 'View Campaign', 'text_domain' ),
			'view_items'            => __( 'View Campaigns', 'text_domain' ),
			'search_items'          => __( 'Search Campaigns', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Items list', 'text_domain' ),
			'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Campaign', 'text_domain' ),
			'description'           => __( 'AJAX searchable campaigns.', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-calendar-alt',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'post',
		);
		register_post_type( 'campaign', $args );

	}
	add_action( 'init', 'cpt_campaigns', 0 );
	// Move Yoast to bottom
	function yoasttobottom() {
		return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
	//Admin styles
	add_action('admin_head', 'adminStyles');
	function adminStyles() {
	  echo '<style>
		.yoast-alert,
		[data-dismissible="ao-img-opt-plug-123"] {
			display: none !important;
		}
		#wp-admin-bar-autoptimize,
		#menu-comments,
		#menu-plugins,
		#menu-tools,
		#menu-appearance,
		#menu-settings,
		#toplevel_page_edit-post_type-acf-field-group {
			display: none;
		}
	  </style>';
	}
	//Admin script
	add_action( 'admin_enqueue_scripts', 'load_custom_script' );
	function load_custom_script() {
		wp_enqueue_script('custom_js_script', get_bloginfo('template_url').'/js/admin.js', array('jquery'));
	}
	//ACF styles
	function my_acf_admin_head() { ?>
    <style type="text/css">
		.acf-flexible-content .layout .acf-fc-layout-handle {
			color: #fff;
			background: #23282d;
		}
		.acf-table tr.acf-row:nth-child(even) .acf-row-handle:first-of-type {
			color: #666;
			text-shadow: none;
			background: #e1e1e1;
		}
		.acf-input img {
			max-width: 100%;
			height: auto;
		}
    </style>
    <?php } add_action('acf/input/admin_head', 'my_acf_admin_head');
?>