<section class="content content-process">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2><?php the_sub_field('section_title_white_tiles'); ?></h2>
				<?php if(get_sub_field('section_desc_white_tiles')) {?>
					<?php the_sub_field('section_desc_white_tiles'); ?>
				<?php } ?>
				
				<?php if( have_rows('white_tiles') ): ?>
					<ul class="row list list-process">
					<?php while( have_rows('white_tiles') ): the_row(); ?>
						<li class="col">
							<div class="list-process-item">
								<img src="<?php the_sub_field('white_tile_icon'); ?>" alt="<?php the_sub_field('white_tile_title'); ?>" />
								<h3><?php the_sub_field('white_tile_title'); ?></h3>
								<?php the_sub_field('white_tile_content'); ?>
							</div>
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>