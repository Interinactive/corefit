<section class="content content-testimonials">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3><?php the_field('stories_title','option'); ?></h3>
				<p><?php the_field('stories_description','option'); ?></p>
			</div>
		</div>
	</div>
	<?php if( have_rows('add_stories', 19) ): ?>
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php while( have_rows('add_stories', 19) ): the_row(); ?>
					<a href="<?php echo esc_url( get_permalink(19) ); ?>" class="swiper-slide">
						<img src="<?php the_sub_field('story_image', 19); ?>" alt="<?php the_sub_field('story_title', 19); ?>" />
						<div class="testimonial-label">
							<?php the_sub_field('story_title', 19); ?>
						</div>
						<blockquote><?php the_sub_field('story_introduction', 19); ?></blockquote>
					</a>
				<?php endwhile; ?>
			</div>
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<a href="<?php echo esc_url( get_permalink(19) ); ?>" class="btn btn-primary"><?php the_field('stories_button_label','option'); ?> <i class="fas fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</section>