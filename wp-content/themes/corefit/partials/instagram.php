<section class="content content-instagram">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h5><?php the_field('instagram_feed_title','option'); ?></h5>
				<h5 class="instagram"><i class="fab fa-instagram"></i> <?php the_field('instagram_feed_handle','option'); ?></h5>
				<ul class="list list-instagram"></ul>
				
				<?php if( get_field('link_instagram','option') ): ?>
					<a class="instagram-follow" href="<?php the_field('link_instagram','option'); ?>">Follow us on instagram <i class="fas fa-chevron-right"></i></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>