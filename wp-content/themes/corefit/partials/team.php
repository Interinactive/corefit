<section class="content content-team">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2>The Team</h2>
				<?php if( have_rows('team_members') ): ?>
					<ul class="row list list-team">
					<?php while( have_rows('team_members') ): the_row(); ?>
						<li>
							<div class="list-team-member">
								<div class="list-team-img">
									<img src="<?php the_sub_field('team_member_photo'); ?>" alt="<?php the_sub_field('team_member_name'); ?>" />
								</div>
								<div class="list-team-desc">
									<h2><?php the_sub_field('team_member_name'); ?></h2>
									<div class="list-team-position">
										<?php the_sub_field('team_member_position'); ?>
									</div>
									<?php the_sub_field('team_member_description'); ?>
								</div>
							</div>
						</li>
					<?php endwhile; ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>