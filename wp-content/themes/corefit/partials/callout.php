<?php $campaign = get_field('choose_a_campaign','option'); ?>
<section class="content content-callout" style="background-image:url(<?php the_field('campaign_image', $campaign); ?>);">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="callout-copy">
					<div class="callout-note">
						<?php the_field('campaign_highlight', $campaign); ?>
					</div>
					<h2 class="upper"><?php the_field('campaign_global_title', $campaign); ?></h2>
					<p><?php the_field('campaign_global_subtitle', $campaign); ?></p>
					<a href="<?php echo get_permalink( $campaign ); ?>" class="btn btn-dark"><?php the_field('campaign_global_button_label', $campaign); ?> <i class="fas fa-chevron-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>