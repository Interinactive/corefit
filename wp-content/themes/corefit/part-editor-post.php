<?php while ( have_rows('editor_content_post') ) : the_row(); ?>
	<?php if( get_row_layout() == 'blog_text' ): ?>
		<?php the_sub_field('blog_text_post'); ?>
	<?php elseif( get_row_layout() == 'blog_video' ): ?>
		<?php $video = get_sub_field( 'blog_video_url' );
		preg_match('/src="(.+?)"/', $video, $matches_url );
		$src = $matches_url[1];
		preg_match('/embed(.*?)?feature/', $src, $matches_id );
		$id = $matches_id[1];
		$id = str_replace( str_split( '?/' ), '', $id );?>
		<div class='embed-container'>
			<iframe src='https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0' frameborder='0' allowfullscreen></iframe>
		</div>
	<?php elseif( get_row_layout() == 'blog_points' ): ?>
		<div class="article-points">
			<h3><?php the_sub_field('blog_points_title'); ?></h3>
			<?php if( have_rows('blog_list_content') ): ?>
				<ul class="list list-article-points">
				<?php while( have_rows('blog_list_content') ): the_row(); ?>
					<li><?php the_sub_field('blog_point'); ?></li>
				<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>
	<?php endif; ?>
<?php	endwhile; ?>