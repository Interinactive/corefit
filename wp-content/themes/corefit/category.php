<?php get_header(); ?>

		<section class="featured-page brand-green-bg">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<span class="page-title">
							<?php  single_cat_title(); ?>
						</span>
						<h1>Health Blog</h1>
						<div class="nav-cat">
							<div class="nav-cat-title">
								Show:
							</div>
							<?php $thisTrueCat = get_category( get_query_var( 'cat' ) ); ?>
							<ul class="list list-categories">
								<li><a href="<?php echo get_bloginfo('url'); ?>/blog/all">All</a>
								<?php
									$thisCat = get_category( get_query_var( 'cat' ) ); 
									while ( intval($thisCat->parent) > 0 ) {
										$thisCat = get_category ( $thisCat->parent );
									}
									$args=array(
										'child_of' => $thisCat->term_id,
										'hide_empty' => 0,
										'orderby' => 'name',
										'order' => 'ASC'
									);
									$categories=get_categories( $args );
									foreach( $categories as $category ) { ?>
										<li <?php if ($thisTrueCat->term_id == $category->term_id) { ?> class="current-cat" <?php } ?>><a href="<?php echo get_category_link( $category->term_id ) ?>" ><?php echo $category->name ?></a></li>  
									<?php
									}; 
								?>
							</ul> 
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="content content-blog content-shift">
			<div class="container">
					<ul class="row list list-blog">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<li class="col-md-4">
								<a href="<?php the_permalink(); ?>" class="list-blog-item">
									<?php 
										$image = get_field('post_featured_image');
										$alt = $image['alt'];
										$size = 'medium';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									?>
									<img src="<?php echo $thumb; ?>" alt="<?php if( $alt ) { echo $alt; } else { the_title(); } ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
									<div class="list-blog-content">
										<div class="list-blog-meta">
											<time datetime="<?php the_date('Y-m-d');?>"><?php the_time('F j, Y');?></time>
											<div class="list-blog-category">
												<?php foreach((get_the_category()) as $category){
													echo $category->name;
												}	?> 
											</div>
										</div>
										<h4><?php if(get_field('post_header', $page->ID) == 'video') { ?><i class="fas fa-play-circle"></i> <?php } ?><?php the_title(); ?></h4>
										<p><?php the_field('post_excerpt'); ?></p>
									</div>
								</a>
							</li>
						 <?php endwhile; endif; ?>
					</ul>
				<?php wpbeginner_numeric_posts_nav(); ?>
			</div>
		</section>
		
		<?php get_template_part( 'partials/callout-blue'); ?>
	
<?php get_footer(); ?>