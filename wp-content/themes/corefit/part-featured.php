<?php if ( is_front_page() ) { ?>
		<section class="featured">
			<video autoplay="" loop="" style="background-image:url(<?php the_field('video_poster_image'); ?>)" muted="" playsinline="">
				<source src="<?php the_field('video_webm'); ?>">
				<source src="<?php the_field('video_mp4'); ?>">
			</video>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1><?php the_field('page_title'); ?></h1>
						<h2><?php the_field('page_subtitle'); ?></h2>
					</div>
				</div>
			</div>
		</section>
	<?php } else { ?>
		<?php 
			$pageColour = "brand-orange-bg";
			if(get_field('page_colour') == 'orange') {
				$pageColour = "brand-orange-bg";
			} elseif(get_field('page_colour') == 'green') {
				$pageColour = "brand-green-bg";
			} elseif(get_field('page_colour') == 'blue') {
				$pageColour = "brand-blue-bg";
			} elseif(get_field('page_colour') == 'navy') {
				$pageColour = "brand-navy-bg";
			} elseif(get_field('page_colour') == 'magenta') {
				$pageColour = "brand-pink-bg";
			} else {
				$pageColour = "brand-blue-bg";
			}
		?>
		<section class="featured-page <?php echo $pageColour; ?>" <?php if(get_field('campaign_image')) {?>style="background-image:url(<?php the_field('campaign_image'); ?>);"<?php } ?>>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<span class="page-title">
							<?php echo get_the_title( $post->post_parent ); ?>
						</span>
						<h1><?php the_title(); ?></h1>
						<?php if(get_field('choice_page_description') == 'yes') {?>
							<?php if ( is_page_template( 'page-program.php' ) ) { ?>
								<div class="row row-program">
									<div class="col-md-8">
										<?php the_field('page_description'); ?>
										
										<?php global $post; if($post->ID == 576) { ?>
											<?php if( have_rows('corporate_logos') ): ?>
												<ul class="list list-companies">
												<?php while( have_rows('corporate_logos') ): the_row(); ?>
													<li>
														<?php 
															$image = get_sub_field('logo_image');
															$alt = $image['alt'];
															$size = 'large';
															$thumb = $image['sizes'][ $size ];
															$width = $image['sizes'][ $size . '-width' ];
															$height = $image['sizes'][ $size . '-height' ];
														?>
														<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
													</li>
												<?php endwhile; ?>
												</ul>
											<?php endif; ?>
										<?php } ?>
										
									</div>
									<div class="col-md-4">
										<?php if( have_rows('program_features') ): ?>
											<ul class="list list-features">
											<?php while( have_rows('program_features') ): the_row(); ?>
												<li>
													<div class="list-features-content">
														<img src="<?php the_sub_field('program_feature_icon'); ?>" alt="<?php the_sub_field('program_feature_label'); ?>">
														<div class="list-features-title">
															<?php the_sub_field('program_feature_label'); ?>
														</div>
													</div>
												</li>
											<?php endwhile; ?>
											</ul>
										<?php endif; ?>
									</div>
								</div>
							<?php } else { ?>
								<?php the_field('page_description'); ?>
								<?php if(get_field('campaign_highlight')) {?>
									<div class="campaign-note">
										<?php the_field('campaign_highlight'); ?>
									</div>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>