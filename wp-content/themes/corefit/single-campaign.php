<?php /* Template Name: Campaign template */ ?>

<?php get_header(); ?>

		<?php get_template_part( 'part-featured'); ?>
		
		<?php get_template_part( 'part-video'); ?>
		
		<?php get_template_part( 'part-editor-campaign'); ?>
		
		<section class="content content-contact">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2><?php the_field('deal_button_label'); ?></h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<?php gravity_form(3, false, false, false, '', true, 12); ?>
					</div>
					<div class="col-md-4">
						<div class="side-item side-item-deal">
							<?php the_field('deal_details'); ?>
						</div>
					</div>
				</div>
			</div>
		</section>

<?php get_footer(); ?>