<?php while ( have_rows('editor_content_program') ) : the_row(); ?>
	<?php if( get_row_layout() == 'section_image_text' ): ?>
		<section class="content content-width content-program-intro">
			<div class="row">
				<div class="col-lg-6" style="background-image: url(<?php the_sub_field('section_image_text_image'); ?>)"></div>
				<div class="col-lg-6">
					<div class="content-width-content">
						<h2 class="title-div"><?php the_sub_field('section_image_text_title'); ?></h2>
						<?php the_sub_field('section_image_text_content'); ?>
						<p class="intro"><?php the_sub_field('section_image_text_list_title'); ?></p>
						<?php if( have_rows('focus_list') ): ?>
							<ul class="list list-features">
							<?php while( have_rows('focus_list') ): the_row(); ?>
								<li>
									<div class="list-features-content">
										<img src="<?php the_sub_field('focus_item_icon'); ?>" alt="<?php the_sub_field('focus_item_label'); ?>">
										<div class="list-features-title">
											<?php the_sub_field('focus_item_label'); ?>
										</div>
									</div>
								</li>
							<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php elseif( get_row_layout() == 'section_program_details' ): ?>
		<section class="content content-program">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2><?php the_sub_field('section_program_details_title'); ?></h2>
					</div>
				</div>
				<?php if( have_rows('section_program_details_row') ): ?>
					<?php while( have_rows('section_program_details_row') ): the_row(); ?>
						<div class="row row-details">
							<div class="col-md-6">
								<?php the_sub_field('left_column_content'); ?>
							</div>
							<div class="col-md-6">
								<?php the_sub_field('right_column_content'); ?>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>
	<?php endif; ?>
<?php	endwhile; ?>