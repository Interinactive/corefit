<?php /* Template Name: Programs template */ ?>

<?php get_header(); ?>

		<?php get_template_part( 'part-featured'); ?>
		<section class="content content-width content-width-program">
			<?php $mypages = get_pages( array( 'child_of' => $post->ID,'parent' => $post->ID, 'sort_column' => 'ID','sort_order' => 'asc' ) );
				foreach( $mypages as $page ) { ?>
				<?php 
					$brandColour = "brand-orange";
					if(get_field('page_colour', $page->ID) == 'orange') {
						$brandColour = "brand-orange";
					} elseif(get_field('page_colour', $page->ID) == 'green') {
						$brandColour = "brand-green";
					} elseif(get_field('page_colour', $page->ID) == 'blue') {
						$brandColour = "brand-blue";
					} elseif(get_field('page_colour', $page->ID) == 'navy') {
						$brandColour = "brand-navy";
					} elseif(get_field('page_colour', $page->ID) == 'magenta') {
						$brandColour = "brand-pink";
					}
				?>
				<div class="row">
					<div class="col-lg-6 col-img" style="background-image: url(<?php the_field('program_image', $page->ID) ?>"></div>
					<div class="col-lg-6">
						<div class="content-width-content">
							<h2 class="title-div"><a class="<?php echo $brandColour; ?>" href="<?php echo get_permalink($page); ?>"><?php echo $page->post_title; ?></a></h2>
							<?php if( have_rows('program_features', $page->ID) ): ?>
								<ul class="list list-features">
								<?php while( have_rows('program_features', $page->ID) ): the_row(); ?>
									<li>
										<div class="list-features-content">
											<img src="<?php the_sub_field('program_feature_icon'); ?>" alt="<?php the_sub_field('program_feature_label'); ?>">
											<div class="list-features-title">
												<?php the_sub_field('program_feature_label'); ?>
											</div>
										</div>
									</li>
								<?php endwhile; ?>
								</ul>
							<?php endif; ?>
							<?php the_field('program_description', $page->ID) ?>
							<a href="<?php echo get_permalink($page); ?>" class="btn btn-primary"><?php the_field('program_button_label', $page->ID) ?> <i class="fas fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			<?php } ?>
		</section>
		
		<?php get_template_part( 'partials/white-tiles'); ?>
		<?php get_template_part( 'partials/instagram'); ?>
		
<?php get_footer(); ?>