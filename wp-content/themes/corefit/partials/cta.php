<?php global $post; if($post->ID == 576) { ?>
	<section class="content content-cta content-cta-navy">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2><?php the_field('cta_corporate_title'); ?></br>
					<span class="brand-white"><?php the_field('cta_corporate_smaller_title'); ?></span></h2>
					<a href="<?php the_field('cta_corporate_button_link'); ?>" class="btn btn-dark"><?php the_field('cta_corporate_button_label'); ?> <i class="fas fa-chevron-right"></i></a>
				</div>
			</div>
		</div>
		<div class="skew"></div>
	</section>
<?php } else { ?>
	<section class="content content-cta">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2><?php the_field('cta_title','option'); ?></br>
					<span class="brand-white"><?php the_field('cta_title_small','option'); ?></span></h2>
					<a href="<?php the_field('cta_button_link','option'); ?>" class="btn btn-dark"><?php the_field('cta_button_label','option'); ?> <i class="fas fa-chevron-right"></i></a>
				</div>
			</div>
		</div>
		<div class="skew"></div>
	</section>
<?php } ?>