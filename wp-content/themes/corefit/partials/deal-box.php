<section class="content content-deal">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php the_sub_field('deal_content'); ?>
				<a href="#" class="btn btn-primary btn-form"><?php the_field('deal_button_label'); ?> <i class="fas fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</section>