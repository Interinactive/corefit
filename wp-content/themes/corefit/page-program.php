<?php /* Template Name: Program template */ ?>

<?php get_header(); ?>

		<?php get_template_part( 'part-featured'); ?>
		<?php get_template_part( 'part-editor-program'); ?>
		<?php get_template_part( 'partials/cta'); ?>
		
		<section class="content content-program-footer">
			<div class="container">
			
				<?php global $post; if( ! is_page('corporate-fitness-newcastle')) { ?>
					<div class="row">
						<div class="col-12">
							<h2 class="text-center">Other programs...</h2>
							<ul class="row list list-programs">
							<?php
								global $post;
								$mypages = get_pages( array(
									'child_of' => 17,
									'parent' => 17,
									'exclude' => $post->ID,
									'orderby' => 'rand',
									'sort_column' => 'ID',
									'sort_order' => 'asc',
									'number' =>2
								) );
								foreach( $mypages as $page ) { ?>
								<?php 
									$brandColour = "brand-orange";
									if(get_field('page_colour', $page->ID) == 'orange') {
										$brandColour = "brand-orange";
									} elseif(get_field('page_colour', $page->ID) == 'green') {
										$brandColour = "brand-green";
									} elseif(get_field('page_colour', $page->ID) == 'blue') {
										$brandColour = "brand-blue";
									} elseif(get_field('page_colour', $page->ID) == 'navy') {
										$brandColour = "brand-navy";
									} elseif(get_field('page_colour', $page->ID) == 'magenta') {
										$brandColour = "brand-pink";
									}
								?>
								<li class="col-md-6">
									<a href="<?php echo get_permalink($page); ?>" class="list-program">
										<div class="list-program-wrap" style="background-image:url(<?php the_field('program_image', $page->ID); ?>);">
											<div class="list-program-content">
												<h3 class="div-<?php echo $brandColour; ?>"><?php echo $page->post_title; ?></h3>
												<?php if( have_rows('program_features', $page->ID) ): ?>
													<ul class="list list-features">
													<?php while( have_rows('program_features', $page->ID) ): the_row(); ?>
														<li>
															<div class="list-features-content">
																<div class="list-features-title">
																	<?php the_sub_field('program_feature_label'); ?>
																</div>
															</div>
														</li>
													<?php endwhile; ?>
													</ul>
												<?php endif; ?>
											</div>
										</div>
									</a>
								</li>
							<?php } ?>
							</ul>
						</div>
					</div>
				<?php } ?>
				
				<div class="row">
					<div class="col-12">
						<h2 class="text-center"><?php the_field('video_footer_title'); ?></h2>
						<?php if(get_field('page_video_url')) {?>
							<?php $video = get_field( 'page_video_url' );
							preg_match('/src="(.+?)"/', $video, $matches_url );
							$src = $matches_url[1];	
							preg_match('/embed(.*?)?feature/', $src, $matches_id );
							$id = $matches_id[1];
							$id = str_replace( str_split( '?/' ), '', $id );?>
							<div class="row">
								<div class="col-12 col-video">
									<div class='embed-container'>
										<iframe src='https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0' frameborder='0' allowfullscreen></iframe>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
				
				
			</div>
		</section>
		
		<?php global $post; if( ! is_page('corporate-fitness-newcastle')) { ?>
			<?php get_template_part( 'partials/callout'); ?>
			<?php get_template_part( 'partials/instagram'); ?>
			<?php get_template_part( 'partials/testimonials'); ?>
		<?php } ?>
		
<?php get_footer(); ?>