<section class="content content-width">
	<div class="row">
		<div class="col-lg-6 col-img" style="background-image: url(<?php the_sub_field('section_image_text_image'); ?>)"></div>
		<div class="col-lg-6">
			<div class="content-width-content">
				<h2 class="title-div"><?php the_sub_field('section_image_text_title'); ?></h2>
				<?php the_sub_field('section_image_text_content'); ?>
			</div>
		</div>
	</div>
</section>