<?php while ( have_rows('editor_content') ) : the_row(); ?>
	<?php if( get_row_layout() == 'section_callout' ): ?>
		<?php get_template_part( 'partials/callout'); ?>
	<?php elseif( get_row_layout() == 'section_callout_blue' ): ?>
		<?php get_template_part( 'partials/callout-blue'); ?>
	<?php elseif( get_row_layout() == 'section_program_tiles' ): ?>
		<section class="content">
			<div class="container">
				<div class="row">
					<?php get_template_part( 'partials/program-tiles'); ?>
				</div>
			</div>
		</section>
	<?php elseif( get_row_layout() == 'section_awards' ): ?>
		<?php get_template_part( 'partials/awards'); ?>
	<?php elseif( get_row_layout() == 'section_testimonials' ): ?>
		<?php get_template_part( 'partials/testimonials'); ?>
	<?php elseif( get_row_layout() == 'section_white_tiles' ): ?>
		<?php get_template_part( 'partials/white-tiles'); ?>
	<?php elseif( get_row_layout() == 'section_team_members' ): ?>
		<?php get_template_part( 'partials/team'); ?>
	<?php elseif( get_row_layout() == 'section_blog_posts' ): ?>
		<?php get_template_part( 'partials/blog-posts'); ?>
	<?php elseif( get_row_layout() == 'section_centered_text' ): ?>
		<?php get_template_part( 'partials/centered'); ?>
	<?php elseif( get_row_layout() == 'section_cta_strip' ): ?>
		<?php get_template_part( 'partials/cta'); ?>
	<?php elseif( get_row_layout() == 'section_instagram' ): ?>
		<?php get_template_part( 'partials/instagram'); ?>
	<?php elseif( get_row_layout() == 'section_image_text' ): ?>
		<?php get_template_part( 'partials/text-image'); ?>
	<?php endif; ?>
<?php	endwhile; ?>