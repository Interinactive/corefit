jQuery(document).ready(function($){
	//Swiper
	var resizeTimer;
	if($(".swiper-container").length) {
		var swiper = new Swiper('.swiper-container', {
			slidesPerView: 'auto',
			centeredSlides: true,
			loop: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		});
	};
	$(window).on('resize', function(e) {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			swiper.update();
		}, 250);
	});
	
	//Instagram
	if($(".list-instagram").length) {
		jQuery.fn.spectragram.accessData = {
			accessToken: '1169077249.fcde9c4.c033a6ac56f6449ea366b4c65e6d21af'
		};
		$('.list-instagram').spectragram('getUserFeed',{
			max: 4,
			size: "medium",
			wrapEachWith: '<li></li>'
		});
	};
	
	//Scroll to form
	$(".btn-form").on("click", function (e) {
		e.preventDefault();
		$.scrollTo($(".content-contact"), 800, {
			'axis':'y'
		});
	});
	
	//Scroll to payment
	$(".btn-payment").on("click", function (e) {
		e.preventDefault();
		$.scrollTo($(".stripe-pay"), 800, {
			'axis':'y'
		});
	});
	
	//Open programs
	var $this = $(".row-drop");
	$(".nav-head-large .menu-item-22").on("click", function (e) {
		e.preventDefault();
		if($this.hasClass("visible")) {
			$this.removeClass("visible");
		} else {
			$this.addClass("visible");
		}
	});
	$(".drop-close").on("click", function (e) {
		e.preventDefault();
		$this.removeClass("visible");
	});
	
	//Mobile nav duplicate
	$(".container-drop ul.list-programs").clone().appendTo(".container-mobile-menu .menu-item-22 ul.sub-menu");
	$(".container-drop .drop-all").clone().appendTo(".container-mobile-menu .menu-item-22 ul.sub-menu ");
	$("header .nav-cta").clone().appendTo(".container-mobile-menu .card-body");
	
	//Open programs mobile
	var $thisMobile = $(".collapse-mobile .sub-menu");
	$(".collapse-mobile .menu-item-22 > a").on("click", function (e) {
		e.preventDefault();
		if($thisMobile.hasClass("visible")) {
			$thisMobile.removeClass("visible");
		} else {
			$thisMobile.addClass("visible");
		}
	});
	
});