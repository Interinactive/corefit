<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'corefit' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Jm](-lhNJhcF;7Qe0cn.D~&/C>tn~Q]oB dx/[cn4-]uv@0xA=1]_5vENGY%f+h>' );
define( 'SECURE_AUTH_KEY',  'ryfKL3s:c?HyI,V?a 39~QB3}x)f_l$r}`#k<RtN4 WAJ; i&LB,6p;/e|]?3gzf' );
define( 'LOGGED_IN_KEY',    'Y[ko-%2k%#Dje&SahPPz[T1@<.[Fsd@Jb*IIj+,iIzXrt.sqY<;dT|L.Q8X&xm9J' );
define( 'NONCE_KEY',        'ZBZ(g.,VBbh}!&k#B,>O+GL)4g9du89p5y_m)rD*GlT&9>p_Gn:Rj)6(N{/h5q:X' );
define( 'AUTH_SALT',        'A~wp;Gp>ojU4 ^>4y*P,ko`)(K~uSK_*ypp;y&P)Mfw0OryNU6]g]kH{8DH0r$$8' );
define( 'SECURE_AUTH_SALT', 'zAOp<FdyA1`( mlZ8hC9QQG-cvF3n/6(//%)wjDLY-LfG3ed(i]%4#$r5|}*-+z]' );
define( 'LOGGED_IN_SALT',   '6#*Tfvrtn*XQYWy5ZnOAe!oRq+b74nsINV,WznOW5~z<aTYaRnJ&4ysj]KBOI?{8' );
define( 'NONCE_SALT',       '};xcyk4;8(bh0hr7F*ajfMI3.3z5T)MmBs;/r$k`Hj)->^(y8)>8v#|<q$@$|J=+' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
