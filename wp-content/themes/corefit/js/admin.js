jQuery(document).ready(function(){
	var hidden = 1;
	var $selectors = "#wp-admin-bar-autoptimize,#menu-appearance,#menu-comments,#menu-plugins,#menu-tools,#menu-settings,#toplevel_page_edit-post_type-acf-field-group";
	jQuery("body").keyup(function(e){
		if(e.keyCode == 192){
			if(hidden==1) {
				jQuery($selectors).css("display","block");
				hidden = 0;
			} else {
				jQuery($selectors).css("display","none");
				hidden = 1;
			}
		};
	});
});