<section class="content content-text">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<?php the_sub_field('centered_text'); ?>
				<a href="#" class="btn btn-primary btn-form"><?php the_field('deal_button_label'); ?> <i class="fas fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
</section>