<?php get_header(); ?>

		<?php get_template_part( 'part-featured'); ?>
		
		<section class="content content-page">
			<div class="container">
				<div class="row justify-content-md-center">
					<div class="col-8">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
		
<?php get_footer(); ?>