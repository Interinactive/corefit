//Dependencies
var gulp = require('gulp'),
autoprefixer = require('gulp-autoprefixer'),
sass = require('gulp-sass'),
uglify = require('gulp-uglify'),
concat = require('gulp-concat'),
cleanCSS = require('gulp-clean-css'),
pxtorem = require('gulp-pxtorem'),
autoprefixer = require('gulp-autoprefixer');

//PX2REM options
var pxtoremOptions = {
    replace: false
};

//JS minify
gulp.task('scripts', function() {
	gulp.src(['src-js/!(script)*.js', 'src-js/*.js'])
	.pipe(concat('script.js'))
	.pipe(uglify())
	.pipe(gulp.dest('../js/'));
});

//Bootstrap changes
gulp.task('minify-bootstrap', function() {
	return gulp.src(['src-css/bootstrap/*.scss'])
	.pipe(sass.sync().on('error', sass.logError))
	.pipe(autoprefixer({ browsers: ['last 1000 versions'], cascade: false }))
	.pipe(cleanCSS())
	.pipe(gulp.dest('src-css/'));
});

//CSS minify
gulp.task('minify-css', function() {
	return gulp.src(['src-css/*.scss'])
	.pipe(pxtorem(pxtoremOptions))
	.pipe(sass.sync().on('error', sass.logError))
	.pipe(autoprefixer({ browsers: ['last 1000 versions'], cascade: false }))
	.pipe(cleanCSS())
	.pipe(gulp.dest('../css/'));
});

//Watch for changes
gulp.task('watch', function () {
	gulp.watch(['src-js/*.js', './lib/*.js'], ['scripts']);
	gulp.watch(['src-css/bootstrap/*.scss'], ['minify-bootstrap']);
	gulp.watch(['src-css/*.scss'], ['minify-css']);
});

//Tasks
gulp.task('default', ['minify-css', 'minify-bootstrap', 'scripts', 'watch']);

//Error handling
function errorHandler (error) {
	console.log(error.toString());
	this.emit('end');
}