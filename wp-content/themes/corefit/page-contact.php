<?php /* Template Name: Contact template */ ?>

<?php get_header(); ?>

		<?php get_template_part( 'part-featured'); ?>
		
		<section class="content content-contact">
			<div class="container">
				<?php while ( have_rows('editor_content_contact') ) : the_row(); ?>
					<?php if( get_row_layout() == 'two_columns_contact' ): ?>
						<div class="row">
							<div class="col-md-4">
								<div class="side-item">
									<?php if(get_sub_field('display_phone_number') == 'yes') { ?>
										<a href="tel:<?php the_field('footer_phone_number','option'); ?>"><i class="fas fa-phone"></i> Call <?php the_field('footer_phone_number','option'); ?></a>
									<?php 	} ?>
									<?php the_sub_field('sidebar_content_contact'); ?>
								</div>
							</div>
							<div class="col-md-8">
								<?php the_sub_field('content_area_contact'); ?>
							</div>
						</div>
					<?php endif; ?>
				<?php	endwhile; ?>
			</div>
		</section>
		
		<?php global $post; if( $post->ID == 499) { ?>
			<?php get_template_part( 'partials/testimonials'); ?>
			<?php get_template_part( 'partials/callout-blue'); ?>
		<?php } else { ?>
			<?php get_template_part( 'partials/instagram'); ?>
			<?php get_template_part( 'partials/callout-blue'); ?>
		<?php } ?>
		
<?php get_footer(); ?>