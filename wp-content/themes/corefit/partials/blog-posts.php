<section class="content content-posts">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3><?php the_field('blog_title','option'); ?></h3>
					<?php $the_query = new WP_Query( array(
						'post__not_in' => array( $post->ID ),
						'posts_per_page' => 3
					)); ?>
					<ul class="row list list-blog">
					<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
						<li class="col-md-4">
							<a href="<?php the_permalink() ?>" class="list-blog-item">
								<?php 
									$image = get_field('post_featured_image');
									$alt = $image['alt'];
									$size = 'medium';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								?>
								<img src="<?php echo $thumb; ?>" alt="<?php if( $alt ) { echo $alt; } else { the_title(); } ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
								<div class="list-blog-content">
									<div class="list-blog-meta">
										<time datetime="<?php the_date('Y-m-d');?>"><?php the_time('F j, Y');?></time>
										<div class="list-blog-category">
											<?php foreach((get_the_category()) as $category){
												echo $category->name;
											} ?> 
										</div>
									</div>
									<h4><?php if(get_field('post_header', $page->ID) == 'video') { ?><i class="fas fa-play-circle"></i> <?php } ?><?php the_title(); ?></h4>
									<p><?php the_field('post_excerpt'); ?></p>
								</div>
							</a>
						</li>
					<?php endwhile; ?>
					</ul>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>