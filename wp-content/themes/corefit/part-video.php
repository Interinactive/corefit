<?php if(get_field('page_video_url')) {?>
	<?php $video = get_field( 'page_video_url' );

	preg_match('/src="(.+?)"/', $video, $matches_url );
	$src = $matches_url[1];	

	preg_match('/embed(.*?)?feature/', $src, $matches_id );
	$id = $matches_id[1];
	$id = str_replace( str_split( '?/' ), '', $id );?>
	<section class="content content-media <?php if ( is_front_page() && is_home() ) {?>content-shift-home<?php } else { ?>content-shift<?php } ?>">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class='embed-container'>
						<iframe src='https://www.youtube.com/embed/<?php echo $id ?>?rel=0&showinfo=0' frameborder='0' allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php } ?>