<?php while ( have_rows('campaign_editor_content') ) : the_row(); ?>
	<?php if( get_row_layout() == 'section_awards' ): ?>
		<?php get_template_part( 'partials/awards'); ?>
	<?php elseif( get_row_layout() == 'section_testimonials' ): ?>
		<?php get_template_part( 'partials/testimonials'); ?>
	<?php elseif( get_row_layout() == 'section_deal_box' ): ?>
		<?php get_template_part( 'partials/deal-box'); ?>
	<?php elseif( get_row_layout() == 'section_white_tiles' ): ?>
		<?php get_template_part( 'partials/white-tiles'); ?>
	<?php elseif( get_row_layout() == 'section_centered_text' ): ?>
		<?php get_template_part( 'partials/centered-campaign'); ?>
	<?php elseif( get_row_layout() == 'section_cta_strip' ): ?>
		<?php get_template_part( 'partials/cta'); ?>
	<?php elseif( get_row_layout() == 'section_image_text' ): ?>
		<?php get_template_part( 'partials/text-image'); ?>
	<?php elseif( get_row_layout() == 'section_program_details' ): ?>
		<section class="content content-program">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h2><?php the_sub_field('section_program_details_title'); ?></h2>
					</div>
				</div>
				<?php if( have_rows('section_program_details_row') ): ?>
					<?php while( have_rows('section_program_details_row') ): the_row(); ?>
						<div class="row row-details">
							<div class="col-md-6">
								<?php the_sub_field('left_column_content'); ?>
							</div>
							<div class="col-md-6">
								<?php the_sub_field('right_column_content'); ?>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
				<div class="row">
					<div class="col-12 col-btn">
						<a href="#" class="btn btn-primary btn-form"><?php the_field('deal_button_label'); ?> <i class="fas fa-chevron-right"></i></a>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
<?php	endwhile; ?>