<section class="content content-callout content-callout-small" style="background-image:url(<?php the_field('callout_image_blue','option'); ?>);">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-6">
				<div class="callout-copy">
					<h2><?php the_field('callout_title_blue','option'); ?></h2>
					<?php the_field('callout_description_blue','option'); ?>
					<a href="<?php the_field('callout_button_link_blue','option'); ?>" class="btn btn-dark"><?php the_field('callout_button_text_blue','option'); ?> <i class="fas fa-chevron-right"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>