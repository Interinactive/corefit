<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php bloginfo('name'); ?> | <?php the_title(''); ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/css/style.css" />
		<link href="<?php echo get_bloginfo('template_url'); ?>/images/icon/favicon-32.png" rel="shortcut icon" type="image/x-icon">
		<link href="<?php echo get_bloginfo('template_url'); ?>/images/icon/favicon-256.png" rel="apple-touch-icon">
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-73407607-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-73407607-1');
		</script>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	
		<div id="breakpoint"></div>
		
		<header>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<a href="#collapseNav" class="nav-mobile nav-mobile-menu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseNav">
							<i class="fas fa-bars"></i>
							<div class="nav-mobile-label">
								Menu
							</div>
						</a>
						<a class="logo" href="<?php echo get_bloginfo('url'); ?>">
							<img width="140" src="<?php the_field('site_logo','option'); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>" />
						</a>
						<nav role="navigation">
							<?php wp_nav_menu( array('theme_location' => 'main_nav', 'container' => '', 'items_wrap' => '<ul class="nav-head nav-head-large">%3$s</ul>' ) ); ?>
						</nav>
						<a class="nav-phone nav-mobile" href="tel:<?php the_field('footer_phone_number','option'); ?>"><i class="fas fa-phone"></i></a>
						<div class="nav-cta">
							<a href="<?php the_field('member_login_link','option'); ?>" class="cta">Member login <i class="fas fa-external-link-square-alt"></i></a>
							<a href="<?php echo get_bloginfo('url'); ?>/book-your-free-consultation" class="btn btn-dark">Book your free consultation <i class="fas fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div class="container container-mobile-menu">
			<div class="row">
				<div class="collapse collapse-mobile" id="collapseNav">
					<div class="card card-body">
						<?php wp_nav_menu( array('theme_location' => 'main_nav', 'container' => '', 'items_wrap' => '<ul class="nav-head">%3$s</ul>' ) ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container container-drop">
			<div class="row row-drop">
				<div class="col-12">
					<div class="nav-drop">
						<div class="nav-drop-content">
							<div class="row">
								<div class="col-12 col-close">
									<a href="#" class="drop-close"><i class="far fa-times"></i></a>
								</div>
							</div>
							<div class="row">
								<?php get_template_part( 'partials/program-tiles'); ?>
							</div>
							<div class="row">
								<div class="col-12">
									<a class="drop-all" href="<?php echo get_bloginfo('url'); ?>/programs">All Programs <i class="far fa-chevron-right"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		
		