<?php get_header(); ?>

	<?php  while ( have_posts() ) : the_post(); ?>
		<article>
			<section class="featured-page brand-green-bg">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<span class="page-title">
								<?php foreach((get_the_category()) as $category){
									echo $category->name;
								}	?> 
							</span>
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</section>
			<section class="content content-media content-shift">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php if(get_field('post_header') == 'video') { ?>
									<?php $video = get_field( 'post_video' );
									preg_match('/src="(.+?)"/', $video, $matches_url );
									$src = $matches_url[1];
									preg_match('/embed(.*?)?feature/', $src, $matches_id );
									$id = $matches_id[1];
									$id = str_replace( str_split( '?/' ), '', $id );?>
									<div class='embed-container'>
										<iframe src='https://www.youtube.com/embed/<?php echo $id ?>' frameborder='0' allowfullscreen></iframe>
									</div>
								<?php } else { ?>
								<div class='embed-image'>
									<?php 
										$image = get_field('post_featured_image');
										$alt = $image['alt'];
										$size = 'large';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
									?>
									<img src="<?php echo $thumb; ?>" alt="<?php if( $alt ) { echo $alt; } else { the_title(); } ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
								</div>
							<?php } ?>
							<div class="article-meta">
								<time datetime="<?php the_date('Y-m-d');?>"><?php the_time('F j, Y');?></time>
								<div class="article-category">
									<?php foreach((get_the_category()) as $category){
										echo $category->name;
									}	?> 
								</div>
							</div>
							<section class="article-content">
								<?php get_template_part( 'part-editor-post'); ?>
								Share
								<?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
							</section>
						</div>
					</div>
				</div>
			</section>
		</article>
	<?php endwhile; ?>
	
	<?php get_template_part( 'partials/blog-posts'); ?>
	<?php get_template_part( 'partials/callout-blue'); ?>
		
<?php get_footer(); ?>




