<div class="col-12">
	<h2 class="text-center"><?php the_field('program_tiles_title','option'); ?></h2>
	
	<ul class="row list list-programs">
	<?php $mypages = get_pages( array( 'child_of' => 17,'parent' => 17, 'sort_column' => 'ID','sort_order' => 'asc' ) );
		foreach( $mypages as $page ) { ?>
		<?php 
			$brandColour = "brand-orange";
			if(get_field('page_colour', $page->ID) == 'orange') {
				$brandColour = "brand-orange";
			} elseif(get_field('page_colour', $page->ID) == 'green') {
				$brandColour = "brand-green";
			} elseif(get_field('page_colour', $page->ID) == 'blue') {
				$brandColour = "brand-blue";
			} elseif(get_field('page_colour', $page->ID) == 'navy') {
				$brandColour = "brand-navy";
			} elseif(get_field('page_colour', $page->ID) == 'magenta') {
				$brandColour = "brand-pink";
			}
		?>
		<li class="col-md-3">
			<a href="<?php echo get_permalink($page); ?>" class="list-program">
				<div class="list-program-wrap" style="background-image:url(<?php the_field('program_image', $page->ID); ?>);">
					<div class="list-program-content">
						<h3 class="div-<?php echo $brandColour; ?>"><?php echo $page->post_title; ?></h3>
						<?php if( have_rows('program_features', $page->ID) ): ?>
							<ul class="list list-features">
							<?php while( have_rows('program_features', $page->ID) ): the_row(); ?>
								<li>
									<div class="list-features-content">
										<div class="list-features-title">
											<?php the_sub_field('program_feature_label'); ?>
										</div>
									</div>
								</li>
							<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</a>
		</li>
	<?php } ?>
	</ul>
</div>