<?php /* Template Name: Success stories template */ ?>

<?php get_header(); ?>

		<?php get_template_part( 'part-featured'); ?>
		
		<?php get_template_part( 'part-video'); ?>
		
		<section class="content content-stories">
			<div class="container">
				<div class="row">
					<div class="col-12">
						
						<?php if( have_rows('add_stories') ): $counter = 0; ?>
							<ul class="list list-stories">
							<?php while( have_rows('add_stories') ): the_row(); ?>
								<?php if ($counter == 2) { ?>
									<?php get_template_part( 'partials/cta'); ?>
								<?php } ?>
								<li>
									<div class="list-story">
										<div class="list-story-img">
											<img src="<?php the_sub_field('story_image'); ?>" alt="<?php the_sub_field('story_title'); ?>" />
										</div>
										<div class="list-story-content">
											<h2><?php the_sub_field('story_title'); ?> <i class="fas fa-arrow-down"></i></h2>
											<p class="intro"><?php the_sub_field('story_introduction'); ?></p>
											<?php the_sub_field('story_description'); ?>
											<a class="story-expand" data-toggle="collapse" href="#collapseStory<?php echo $counter;?>" role="button" aria-expanded="false" aria-controls="collapseStory<?php echo $counter;?>"><i class="fal fa-plus"></i><i class="fal fa-minus"></i> More about this story</a>
											<div class="collapse" id="collapseStory<?php echo $counter;?>">
												<div class="story-expanded">
													<?php the_sub_field('story_extended'); ?>
												</div>
											</div>
										</div>
									</div>
								</li>
							<?php $counter++; endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
		
	<?php get_template_part( 'partials/callout'); ?>
	<?php get_template_part( 'partials/callout-blue'); ?>
		
<?php get_footer(); ?>




