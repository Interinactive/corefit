<section class="content content-awards">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3><?php the_field('awards_title','option'); ?></h3>
					<?php if( have_rows('add_awards','option') ): ?>
						<div class="row">
						<?php while( have_rows('add_awards','option') ): the_row(); ?>
							<div class="col-md-4">
								<img src="<?php the_sub_field('award_image','option'); ?>" alt="<?php the_sub_field('award_label','option'); ?>" />
								<h4><?php the_sub_field('award_label','option'); ?></h4>
							</div>
						<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
		</div>
	</div>
</section>