<section class="content content-text">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<?php the_sub_field('centered_text'); ?>
				<a href="<?php the_sub_field('centered_text_button_url'); ?>" class="btn btn-primary"><?php the_sub_field('centered_text_button_label'); ?> <i class="fas fa-chevron-right"></i></a>
				<img class="icon-core" width="75" src="<?php echo get_bloginfo('template_url'); ?>/images/logo-core.svg" alt="Core" />
			</div>
		</div>
	</div>
</section>